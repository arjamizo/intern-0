import { buildCustomerObj } from "./customer.model" // js not required on browser-side due to importmaps and experimental-resolution on nodejs

export function crmRoutes(app) {
  app.get('/api/timeline/v2/object/Contacts/:id', async (req, res) => {
    const customerKeyId = 'id';
    let id = req.params[customerKeyId]
    res.header('Access-Control-Allow-Origin', '*')
    res.send(/*window.*/buildCustomerObj(id))
  })

  app.get('/api/timeline/v2/object/Contacts/751', (req, res) => {
    console.log('wywoluje')
    res.header('Access-Control-Allow-Origin', '*')
    res.send()
  })

  app.post('/api/graphql/crm', async (req, res) => {

    res.header('Access-Control-Allow-Origin', '*')
    const fs = await import('fs') // here so browser can import this file, too
    fs.readFile("./hubmockapi/crm/contacts.json", 'utf-8', (err, data) => {
      if (err) throw { err, cwd: process.cwd() };
      
      data = JSON.parse(data)
      
      let ids = data[0].data.crmObjectsSearch.results.map(el => el.id)
      res.send(ids)
    })
  })
}

// if (window !== undefined) window.crmRoutes = crmRoutes // ReferenceError: window is not defined
if (typeof window !== 'undefined') window.crmRoutes = crmRoutes

console.log('init crm routes');
