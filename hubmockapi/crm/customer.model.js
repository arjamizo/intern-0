import { buildObject } from "../events/event.model.js" // TODO find a way to handle both server-side and client-side

export const objTemplate = {
  "events": [
    {
      "timestamp": 1686178962947,
      "eventData": {
        "sessionStartTimestamp": 1686178828273,
        "sessionEndTimestamp": 1686178962947,
        "firstActivities": [
          {
            "timestamp": 1686178838068,
            "type": "VISIT",
            "userAgent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36",
            "ownerId": 139672907,
            "url": "http://127.0.0.1:3001/cart",
            "title": "Kup mnie taki a taki bilet",
            "property": [],
            "identity": [],
            "city": "warsaw",
            "region": "14",
            "country": "pl",
            "hashId": 531447,
            "uuid": "1fbd3a1a-0e8f-3fc0-8d86-1d8a6b5d6295",
            "vid": 751,
            "processedTimestamp": 1686178838902,
            "activityIds": [],
            "logLineTimestamp": 1686178838068,
            "matchedEventIds": [],
            "newCookie": false,
            "unprocessedMarkers": [],
            "eventTags": [],
            "isContact": false,
            "matchedTargetedContentWidgets": [],
            "browserFingerprint": "2937828053",
            "vidsToMerge": [],
            "leviathanLinkedVids": [],
            "matchedCampaignAndAssets": [],
            "isVirtualUrl": true,
            "companyData": {
              "companyDomain": "orange.pl"
            },
            "visitorDevice": {
              "uaClass": "Browser",
              "uaVersionMajor": "114",
              "uaFamily": "Chrome",
              "osFamily": "Windows",
              "deviceClass": "Desktop"
            }
          },
          {
            "timestamp": 1686178839132,
            "type": "VISIT",
            "userAgent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36",
            "ownerId": 139672907,
            "url": "http://127.0.0.1:3001/cart",
            "referrer": "http://127.0.0.1:3001/cart",
            "title": "Kup mnie taki a taki bilet",
            "property": [],
            "identity": [],
            "city": "warsaw",
            "region": "14",
            "country": "pl",
            "hashId": 561187,
            "uuid": "c94be440-7d8c-3426-99be-10dffb4dd766",
            "vid": 751,
            "processedTimestamp": 1686178840005,
            "activityIds": [],
            "logLineTimestamp": 1686178839132,
            "matchedEventIds": [],
            "newCookie": false,
            "unprocessedMarkers": [],
            "eventTags": [],
            "isContact": false,
            "matchedTargetedContentWidgets": [],
            "browserFingerprint": "2937828053",
            "vidsToMerge": [],
            "leviathanLinkedVids": [],
            "matchedCampaignAndAssets": [],
            "isVirtualUrl": true,
            "isVirtualReferrer": true,
            "companyData": {
              "companyDomain": "orange.pl"
            },
            "visitorDevice": {
              "uaClass": "Browser",
              "uaVersionMajor": "114",
              "uaFamily": "Chrome",
              "osFamily": "Windows",
              "deviceClass": "Desktop"
            }
          },
          {
            "timestamp": 1686178877801,
            "type": "VISIT",
            "userAgent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36",
            "ownerId": 139672907,
            "url": "http://127.0.0.1:3001/cart",
            "referrer": "http://127.0.0.1:3001/cart",
            "title": "Kup mnie taki a taki bilet",
            "property": [],
            "identity": [],
            "city": "warsaw",
            "region": "14",
            "country": "pl",
            "hashId": 653749,
            "uuid": "1ddea5ed-d405-3ea5-845e-d1d03f0045a6",
            "vid": 751,
            "processedTimestamp": 1686178878535,
            "activityIds": [],
            "logLineTimestamp": 1686178877801,
            "matchedEventIds": [],
            "newCookie": false,
            "unprocessedMarkers": [],
            "eventTags": [],
            "isContact": true,
            "matchedTargetedContentWidgets": [],
            "browserFingerprint": "2937828053",
            "vidsToMerge": [],
            "leviathanLinkedVids": [],
            "matchedCampaignAndAssets": [],
            "isVirtualUrl": true,
            "isVirtualReferrer": true,
            "companyData": {
              "companyDomain": "orange.pl"
            },
            "visitorDevice": {
              "uaClass": "Browser",
              "uaVersionMajor": "114",
              "uaFamily": "Chrome",
              "osFamily": "Windows",
              "deviceClass": "Desktop"
            }
          }
        ],
        "totalActivityCount": 6,
        "hasMoreActivities": true,
        "nextTimestamp": 1686178962947
      },
      "etype": "eventPageView",
      "id": "eventPageView-1686178962947-1187915851",
      "orderingId": "TzzyRqTasTk+++++FgscGk"
    },
    {
      "timestamp": 1686178897514,
      "eventData": {
        "time": 1686178897514,
        "logEventName": "ENROLLED",
        "contactName": null,
        "workflowId": 9452117,
        "workflowName": "Unnamed workflow - Thu Jun 1 2023 00:28:19 GMT+0000",
        "vid": 751,
        "enrollmentCount": null,
        "isCanonicalVid": null,
        "stepId": 0,
        "actionName": "",
        "actionType": "",
        "severity": "INFO",
        "isEnrollmentTest": false,
        "triggerSet": [],
        "message": null,
        "metaData": {},
        "contact": null,
        "action": null,
        "id": 0
      },
      "etype": "eventAutomation",
      "id": "eventAutomation-1686178897514-222920462",
      "orderingId": "TzzyRqTbsNI+++++1IZz1U"
    },
    {
      "timestamp": 1686178894753,
      "eventData": {
        "source": {
          "value": "DIRECT_TRAFFIC",
          "timestamp": 1686178894753,
          "selected": true,
          "source-type": "ANALYTICS",
          "source-label": null,
          "source-vids": null,
          "source-id": "ContactAnalyticsDetailsUpdateWorker",
          "updated-by-user-id": null
        }
      },
      "etype": "eventBecameContact",
      "id": "eventBecameContact-1686178894753-2147483647",
      "orderingId": "TzzyRqTbv3s+++++Tzzzzk"
    },
    {
      "timestamp": 1686178878543,
      "eventData": {
        "value": "lead",
        "timestamp": 1686178878543,
        "selected": true,
        "source-type": "ANALYTICS",
        "source-label": null,
        "source-vids": null,
        "source-id": "",
        "updated-by-user-id": null
      },
      "etype": "eventLifecycleStage",
      "id": "eventLifecycleStage-1686178878543-320629750",
      "orderingId": "TzzyRqTc8v++++++2llfxU"
    }
  ],
  "failed": [],
  "timedOut": [],
  "shortCircuited": [],
  "hasMore": false,
  "defaultTimelineTypes": [
    "SEQUENCES",
    "LIST_MEMBERSHIPS",
    "ENGAGEMENTS_CALLS",
    "ASSOCIATED_TICKETS",
    "ENGAGEMENTS_TASKS",
    "ENGAGEMENTS_WHATS_APP",
    "FORM_SUBMISSIONS",
    "ENGAGEMENTS_NOTES",
    "ASSOCIATED_DEALS",
    "ENGAGEMENTS_POSTAL_MAIL",
    "PRESENTATIONS",
    "AD_INTERACTIONS",
    "EMAIL_SENDS",
    "MERGES",
    "ENGAGEMENTS_SMS",
    "EMAIL_OPTOUTS",
    "SIDEKICK",
    "WORKFLOWS",
    "ENGAGEMENTS_CONVERSATION_SESSIONS",
    "EMAIL_UNBOUNCES",
    "ENGAGEMENTS_LINKEDIN_MESSAGE",
    "PAGE_VIEWS",
    "MEDIA_PLAYS",
    "ENGAGEMENTS_MEETINGS",
    "ENGAGEMENTS_EMAILS_TRIMMED",
    "MATCHED_EVENTS",
    "BEHAVIORAL_EVENTS",
    "CONTACT",
    "MARKETING_EVENTS"
  ]
}

/**
 * CRM customer object data builder, see `objTemplate` for structure.
 * 
 * @param {*} vid - visitor id
 * @returns objTemplate - JSON with customer page views activity
 */
export function buildCustomerObj(vid) {
  // export function buildCustomerObj(vid, { buildObject = window.buildObject }) {
  // export function buildCustomerObj(vid, { buildObject } = { buildObject: window.buildObject }) {
  
  let activity = objTemplate.events.filter(el => el.etype === "eventPageView")
  activity.forEach(el => {
    console.log(el)
    el.eventData.firstActivities = el.eventData.firstActivities.map((element) => /*window.*/buildObject(vid, element.url))
  })
  console.log(activity[0].eventData.firstActivities)
  return objTemplate
}

if (typeof window !== 'undefined') {
  window.buildCustomerObj = buildCustomerObj
}
