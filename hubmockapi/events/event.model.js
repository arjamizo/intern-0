export const buildEventObject = (vid, url) => {
  return {
    "timestamp": 1686178828273,
    "type": "VISIT",
    "userAgent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36",
    "ownerId": 139672907,
    "url": url,
    "title": "Kup mnie taki a taki bilet",
    "property": [],
    "identity": [],
    "city": "warsaw",
    "region": "14",
    "country": "pl",
    "hashId": 631032,
    "uuid": "3dfc7868-8361-3ca4-a1d7-e098961a568b",
    "vid": vid,
    "processedTimestamp": 1686178829255,
    "activityIds": [],
    "logLineTimestamp": 1686178828273,
    "matchedEventIds": [],
    "newCookie": true,
    "unprocessedMarkers": [],
    "eventTags": [],
    "isContact": false,
    "matchedTargetedContentWidgets": [],
    "browserFingerprint": "2937828053",
    "vidsToMerge": [],
    "leviathanLinkedVids": [],
    "matchedCampaignAndAssets": [],
    "companyData": {
      "companyDomain": "orange.pl"
    },
    "visitorDevice": {
      "uaClass": "Browser",
      "uaVersionMajor": "114",
      "uaFamily": "Chrome",
      "osFamily": "Windows",
      "deviceClass": "Desktop"
    }
  }
}

/** @deprecated use buildEventObject */
export const buildObject = buildEventObject
if (typeof window !== 'undefined') {
  window.buildObject = buildObject

  window.buildEventObject = buildEventObject
}
