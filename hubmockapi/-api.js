
const apiTracking = () => {
  return {
    identify() {},
    trackPageView() {},
  }
}

const apiData = () => {
  return {
    timelineContacts() {},
    timelineContactDetails() {},
    graphQLCrm() {},
  }
}

window.apiTracking = apiTracking;
window.apiData = apiData;
