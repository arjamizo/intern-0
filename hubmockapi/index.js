import express from 'express'
import fs from 'fs'
import { trackingRoutes } from './tracking/routes.js'
import { crmRoutes } from './crm/routes.js'
const app = express()

function registerEmail(email) {
  users.push({ email, activity: [], otherParams: [] })
}

crmRoutes(app)
trackingRoutes(app)

app.listen(3000, () => {
  console.log("running...")
})
