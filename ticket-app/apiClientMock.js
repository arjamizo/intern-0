// import { crmRoutes } from "../hubmockapi/crm/routes"

/**
 * Wartość pomocnicza umożliwiająca wyłuskanie wartości zwróconej przez handler requestów ExpressJS.
 * Usunąć w momencie odpowiedniego wyeksponowania logiki obsługi requestów w app opartej na ExpressJS.
 */
let resp = undefined;

const _catchingRoutes = {
  get() {},
  post(route, handler) {
    if (route === '/api/graphql/crm') {
      const req = {}
      const res = {
        header() {},
        send(content) {
          resp = content
        },
      }
      // handler(req, res) // WARN relies on require('fs')
      // return fetch('../hubmockapi/crm/contacts.json').then(r => r.json())
      resp = fetch('../hubmockapi/crm/contacts.json').then(r => r.json())
    } else {
      throw 'not impl: ' + route
    }
  },
}

function buildApiClient() {
  return {
    fetchContacts() {
      window.crmRoutes(_catchingRoutes)
      // return resp // Promise.resolve(resp)
      return resp.then(r => r[0].data.crmObjectsSearch.results.map(el => el.id))
      /*return fetch(`http://localhost:3000/api/graphql/crm`, { "method": "POST" })
      .then(response => response.json())*/
    },
    fetchContactById(userId) {
      return Promise.resolve(window.buildCustomerObj(userId))
    },
  }
}

if (localStorage['MOCK_API']) {
  window.apiClient = buildApiClient(`NO_REAL_API_USED`)
}
