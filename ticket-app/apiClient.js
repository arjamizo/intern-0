function buildApiClient() {
  return {
    fetchContacts () {
      return fetch(`http://localhost:3000/api/graphql/crm`, { "method": "POST" })
        .then(response => response.json())
    },
    fetchContactById(userId) {
      return fetch(`http://localhost:3000/api/timeline/v2/object/Contacts/${userId}`, { "method": "GET" })
        .then(response => response.json())
    },
  }
}

window.apiClient = buildApiClient(`http://localhost:3000/api`)
