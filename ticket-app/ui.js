const ui = {
  renderContacts(views, created, activity, createdContactEvent) {
    // pageViewsHtml = `<ul>`
    let pageViewsHtml = `<ul>`
    activity.forEach((em) => {
      em.eventData.firstActivities.forEach(el => {
        let titleStr = `${JSON.stringify(el, null, 2)}`
        pageViewsHtml += `<li title='${titleStr}'> ${el.title} (${el.url}) </li>`
        // document.querySelector('#page-views').innerHTML = pageViewsHtml
        views.innerHTML = pageViewsHtml
      })
    })
    pageViewsHtml += '</ul>'
    
    let createdDate = new Date(createdContactEvent[0].timestamp)
    console.log(createdContactEvent)

    // contactCreatedHtml += `<h1>Contact created</h1><br />${createdDate}`
    // window.contactCreatedHtml += `<h1>Contact created</h1><br />${createdDate}`
    window.contactCreatedHtml = (window.contactCreatedHtml || '') + `<h1>Contact created</h1><br />${createdDate}`
    // document.querySelector('#contact-created').innerHTML = contactCreatedHtml
    created.innerHTML = contactCreatedHtml
  },
}

window.ui = ui;
